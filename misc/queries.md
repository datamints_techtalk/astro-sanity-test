# Sanity queries

## All movies (overview)

*[_type == "movie"] {title, slug, image}

## All movies (details)

*[_type == "movie"] {..., "cast": castMembers[]{..., "actor": person->name}, "crew": crewMembers[]{..., "name": person->name}}
