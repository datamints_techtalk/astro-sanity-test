# Snippets

## Block rendering

import blocksToHtml from '@sanity/block-content-to-html';
const text = blocksToHtml({
    blocks: movie.overview
});

## Image handling

<img src={image.width(400).flipHorizontal().url()} alt={movie.title} />
