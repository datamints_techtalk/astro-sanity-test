import dotenv from 'dotenv';
import PicoSanity from 'picosanity/lib';
import imageUrlBuilder from '@sanity/image-url'

// Load environment.
dotenv.config({ path: '.env' });

// Export Sanity client.
export const sanityClient = new PicoSanity({
  projectId: process.env.SANITY_PROJECT_ID,
  dataset: process.env.SANITY_DATASET,
  apiVersion: '2021-10-07',
  token: process.env.SANITY_TOKEN,
  useCdn: false,
});

// Export utility for image building.
const imageBuilder = imageUrlBuilder(sanityClient);
export const buildImage = image => imageBuilder.image(image);
